use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::{ HashMap, HashSet, VecDeque };

#[derive(Debug, Eq, PartialEq, Hash ,Copy, Clone)]
struct Vec3 {
    x: i32,
    y: i32,
    z: i32,
}

impl std::ops::Sub for Vec3 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        return Self::Output {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        };
    }
}

impl Vec3 {
    fn dist(&self, other: &Self) -> i32 {
        let dx = (self.x - other.x).abs();
        let dy = (self.y - other.y).abs();
        let dz = (self.z - other.z).abs();
        dx * dx + dy * dy + dz * dz
    }

    fn manhattan_dist(&self, other: &Self) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs() + (self.z - other.z).abs()
    }

    fn transform(&self, rotation: [(usize, i32); 3], translation: [i32; 3]) -> Self {
        let v = self.to_vec();
        let new = rotation.iter().zip(translation).map(|((index, sign), diff)| {
            v.get(*index).unwrap() * sign + diff
        }).collect::<Vec<_>>();

        Self::from_vec(new)
    }

    fn to_vec(&self) -> Vec<i32> {
        vec![self.x, self.y, self.z]
    }

    fn from_vec(v: Vec<i32>) -> Self {
        assert_eq!(v.len(), 3, "from_vec received {} numbers", v.len());
        Self { x: v[0], y: v[1], z: v[2] }
    }
}

#[derive(Debug, Clone)]
struct Scanner {
    num: usize,
    pos: Option<Vec3>,
    beacons: HashMap<Vec3, HashSet<i32>>,
}

impl Scanner {
    fn from(num: usize, lines: Vec<&str>) -> Scanner {
        let vectors: Vec<_> = lines.iter().map(|line| {
            let nums: Vec<&str> = line.split(",").collect();
            Vec3 {
                x: nums[0].parse::<i32>().unwrap(),
                y: nums[1].parse::<i32>().unwrap(),
                z: nums[2].parse::<i32>().unwrap(),
            }
        }).collect();
        let mut beacons = HashMap::new();
        for &v in &vectors {
            let mut dists = vectors.iter()
                .map(|other| v.dist(other))
                .collect::<HashSet<_>>();
            dists.remove(&0);
            beacons.insert(v, dists);
        }
        Scanner { num, pos: None, beacons }
    }

    fn overlap_with(&self, other: &Self) -> HashMap<Vec3, Vec3> {
        HashMap::from_iter(self.beacons.iter()
            .flat_map(|b| {
                other.beacons.iter().map(move |o| (b, o))
            })
            .filter(|((_, d1), (_, d2))|
                {
                    d1.intersection(d2).count() >= 11
                })
            .map(|((&b1, _), (&b2, _))| (b1, b2))
        )
    }

    fn normalized(&self, mapping: HashMap<Vec3, Vec3>) -> Scanner {
        let mut iter = mapping.iter();
        let (&ref1, &point1) = iter.next().unwrap();
        let (&ref2, &point2) = iter.next().unwrap();

        let ref_vec = (ref2 - ref1).to_vec();
        let cur_vec = (point2 - point1).to_vec();

        let mut rotation = [(0, 0); 3];
        for (i, ref_value) in ref_vec.iter().enumerate() {
            let (idx, value) = cur_vec.iter().enumerate()
                .find(|(_, value)| value.abs() == ref_value.abs()).unwrap();
            rotation[i] = (idx, value / ref_value);
        }
        let rotated = point1.transform(rotation, [0; 3]);
        let translation = [
            ref1.x - rotated.x,
            ref1.y - rotated.y,
            ref1.z - rotated.z,
        ];

        for (&r, orig) in mapping.iter() {
            assert_eq!(orig.transform(rotation, translation), r);
        }

        let new_beacons = self.beacons.iter()
            .map(|(v, dists)| (v.transform(rotation, translation), dists.clone()))
            .collect();
        Self { num: self.num, pos: Some(Vec3::from_vec(translation.to_vec())), beacons: new_beacons }
    }
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("19".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let input = input.join("\n");

    let mut scanners = VecDeque::from(parse_input(input));
    let mut init_scanner = scanners.pop_front().unwrap();
    init_scanner.pos = Some(Vec3::from_vec(vec![0, 0, 0]));
    let mut known = vec![init_scanner];
    while !scanners.is_empty() {
        'search: for k in known.clone().iter() {
            for (i, s) in scanners.clone().iter().enumerate() {
                let overlap = k.overlap_with(s);
                if overlap.len() >= 12 {
                    let new_s = s.normalized(overlap.clone());
                    for (orig, new) in overlap.clone() {
                        assert!(new_s.beacons.contains_key(&orig));
                        assert_eq!(s.beacons.get(&new).unwrap(), new_s.beacons.get(&orig).unwrap());
                    }
                    known.push(new_s);
                    scanners.remove(i);
                    break 'search;
                }
            }
        }
    }

    let count = known.iter()
        .flat_map(|s| s.beacons.keys())
        .collect::<HashSet<_>>()
        .len() as i32;
    println!("{:?}", count);
    let known = known.iter()
        .map(|s| match s.pos {
            Some(p) => p,
            None => unreachable!(),
        })
        .collect::<Vec<_>>();
    let mut result = 0;
    for i in 0..known.len() {
        for j in i + 1..known.len() {
            let a = known.get(i).unwrap();
            let b = known.get(j).unwrap();
            let dist = a.manhattan_dist(b);
            result = result.max(dist);
        }
    }

    println!("{}", result);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let input = input.join("\n");

    let mut scanners = VecDeque::from(parse_input(input));
    let mut init_scanner = scanners.pop_front().unwrap();
    init_scanner.pos = Some(Vec3::from_vec(vec![0, 0, 0]));
    let mut known = vec![init_scanner];
    while !scanners.is_empty() {
        'outer: for k in known.clone().iter() {
            for (i, s) in scanners.clone().iter().enumerate() {
                let overlap = k.overlap_with(s);
                if overlap.len() >= 12 {
                    let new_s = s.normalized(overlap.clone());
                    for (orig, new) in overlap.clone() {
                        assert!(new_s.beacons.contains_key(&orig));
                        assert_eq!(s.beacons.get(&new).unwrap(), new_s.beacons.get(&orig).unwrap());
                    }
                    known.push(new_s);
                    scanners.remove(i);
                    break 'outer;
                }
            }
        }
    }

    let known = known.iter()
        .map(|s| match s.pos {
            Some(p) => p,
            None => unreachable!(),
        })
        .collect::<Vec<_>>();
    let mut result = 0;
    for i in 0..known.len() {
        for j in i + 1..known.len() {
            let a = known.get(i).unwrap();
            let b = known.get(j).unwrap();
            let dist = a.manhattan_dist(b);
            result = result.max(dist);
        }
    }

    println!("{}", result);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn parse_input(input: String) -> Vec<Scanner> {
    input.split("\n\n").enumerate()
        .map(|(i, chunk)| {
            let lines = chunk.lines()
                .filter(|line| !line.starts_with("---"))
                .collect::<Vec<_>>();
            Scanner::from(i, lines)
        })
        .collect::<Vec<_>>()
}