use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::{ HashMap };

const STEPS_PART_ONE: i32 = 10;
const STEPS_PART_TWO: i32 = 40;

pub fn run() {
    let input: Vec<String> = input::fetch_input("14".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut iter = input.into_iter();
    let mut rule_map: HashMap<String, char> = HashMap::new();
    let mut pair_count: HashMap<String, u64> = HashMap::new();
    let mut element_count: HashMap<char, u64> = HashMap::new();
    let polymer_template = iter.next().unwrap().trim();
    let mut j = 1;
    let polymer_chars: Vec<char> = polymer_template.chars().collect();
    for i in 0..polymer_chars.len() - 1 {
        let pair = format!("{}{}", 
            polymer_chars[i],
            polymer_chars[j]
        );
        if pair_count.contains_key(&pair) {
            *pair_count.get_mut(&pair).unwrap() += 1;
        } else {
            pair_count.insert(pair, 1);
        }
        j += 1;
    }
    for c in polymer_chars {
        if element_count.contains_key(&c) {
            *element_count.get_mut(&c).unwrap() += 1;
        } else {
            element_count.insert(c, 1);
        }
    }
    iter.next();
    'rules: loop {
        let line = iter.next();
        if  line == None {
            break 'rules;
        }
        let line = line.unwrap().trim();
        let rule: Vec<&str> = line.split(" -> ").collect();
        let rule_output: char = rule[1].chars().collect::<Vec<char>>()[0];
        rule_map.insert(rule[0].to_string(), rule_output);
    }
    for _i in 0..STEPS_PART_ONE {
        let pair_count_clone = pair_count.clone();
        for (pair, count) in pair_count_clone.into_iter() {
            let pair_chars: Vec<char> = pair.chars().collect();
            let rule_output = rule_map.get(&pair).unwrap();
            let front_pair: String = Vec::from([pair_chars[0], *rule_output]).into_iter().collect();
            let back_pair: String = Vec::from([*rule_output, pair_chars[1]]).into_iter().collect();
            if pair_count.contains_key(&pair) {
                *pair_count.get_mut(&pair).unwrap() -= count;
            }
            if pair_count.contains_key(&front_pair) {
                *pair_count.get_mut(&front_pair).unwrap() += count;
            } else {
                pair_count.insert(front_pair, count);
            }
            if pair_count.contains_key(&back_pair) {
                *pair_count.get_mut(&back_pair).unwrap() += count;
            } else {
                pair_count.insert(back_pair, count);
            }
            if element_count.contains_key(&rule_output) {
                *element_count.get_mut(&rule_output).unwrap() += count;
            } else {
                element_count.insert(*rule_output, count);
            }
        }
    }
    let mut least_common_element: (Option<char>, u64) = (None, u64::MAX);
    let mut most_common_element: (Option<char>, u64) = (None, 0);
    for (element, count) in element_count.iter() {
        if count > &most_common_element.1 {
            most_common_element = (Some(*element), *count);
        }
        if &least_common_element.1 > count {
            least_common_element = (Some(*element), *count);
        }
    }
    println!("{}", most_common_element.1 - least_common_element.1);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut iter = input.into_iter();
    let mut rule_map: HashMap<String, char> = HashMap::new();
    let mut pair_count: HashMap<String, u64> = HashMap::new();
    let mut element_count: HashMap<char, u64> = HashMap::new();
    let polymer_template = iter.next().unwrap().trim();
    let mut j = 1;
    let polymer_chars: Vec<char> = polymer_template.chars().collect();
    for i in 0..polymer_chars.len() - 1 {
        let pair = format!("{}{}", 
            polymer_chars[i],
            polymer_chars[j]
        );
        if pair_count.contains_key(&pair) {
            *pair_count.get_mut(&pair).unwrap() += 1;
        } else {
            pair_count.insert(pair, 1);
        }
        j += 1;
    }
    for c in polymer_chars {
        if element_count.contains_key(&c) {
            *element_count.get_mut(&c).unwrap() += 1;
        } else {
            element_count.insert(c, 1);
        }
    }
    iter.next();
    'rules: loop {
        let line = iter.next();
        if  line == None {
            break 'rules;
        }
        let line = line.unwrap().trim();
        let rule: Vec<&str> = line.split(" -> ").collect();
        let rule_output: char = rule[1].chars().collect::<Vec<char>>()[0];
        rule_map.insert(rule[0].to_string(), rule_output);
    }
    for _i in 0..STEPS_PART_TWO {
        let pair_count_clone = pair_count.clone();
        for (pair, count) in pair_count_clone.into_iter() {
            let pair_chars: Vec<char> = pair.chars().collect();
            let rule_output = rule_map.get(&pair).unwrap();
            let front_pair: String = Vec::from([pair_chars[0], *rule_output]).into_iter().collect();
            let back_pair: String = Vec::from([*rule_output, pair_chars[1]]).into_iter().collect();
            if pair_count.contains_key(&pair) {
                *pair_count.get_mut(&pair).unwrap() -= count;
            }
            if pair_count.contains_key(&front_pair) {
                *pair_count.get_mut(&front_pair).unwrap() += count;
            } else {
                pair_count.insert(front_pair, count);
            }
            if pair_count.contains_key(&back_pair) {
                *pair_count.get_mut(&back_pair).unwrap() += count;
            } else {
                pair_count.insert(back_pair, count);
            }
            if element_count.contains_key(&rule_output) {
                *element_count.get_mut(&rule_output).unwrap() += count;
            } else {
                element_count.insert(*rule_output, count);
            }
        }
    }
    let mut least_common_element: (Option<char>, u64) = (None, u64::MAX);
    let mut most_common_element: (Option<char>, u64) = (None, 0);
    for (element, count) in element_count.iter() {
        if count > &most_common_element.1 {
            most_common_element = (Some(*element), *count);
        }
        if &least_common_element.1 > count {
            least_common_element = (Some(*element), *count);
        }
    }
    println!("{}", most_common_element.1 - least_common_element.1);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
