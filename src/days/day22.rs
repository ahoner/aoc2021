use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::HashSet;

#[derive(Debug)]
enum Command {
    On(((i128, i128), (i128, i128), (i128, i128))),
    Off(((i128, i128), (i128, i128), (i128, i128)))
}

#[derive(Clone, Debug, Hash, Eq, PartialEq)]
struct Vec3 {
    x: i128,
    y: i128,
    z: i128
}


#[derive(Clone, Debug, Hash, Eq, PartialEq)]
struct Cube {
    start: Vec3,
    end: Vec3
}

impl Cube {
    fn count(&self) -> i128 {
        return ((self.end.x - self.start.x) * (self.end.y - self.start.y) * (self.end.z - self.start.z)) as i128;
    }

}

fn subtract(cube_1: &Cube, cube_2: &Cube) -> Vec<Cube> {
    let mut new_cubes = Vec::new();
    if !(cube_2.end.x > cube_1.start.x && cube_1.end.x > cube_2.start.x &&
        cube_2.end.y > cube_1.start.y && cube_1.end.y > cube_2.start.y &&
        cube_2.end.z > cube_1.start.z && cube_1.end.z > cube_2.start.z) {
        new_cubes.push(cube_1.clone());
    } else {
        let cube_2 = &Cube {
            start: Vec3 {
                x: cube_1.start.x.max(cube_2.start.x).min(cube_1.end.x),
                y: cube_1.start.y.max(cube_2.start.y).min(cube_1.end.y),
                z: cube_1.start.z.max(cube_2.start.z).min(cube_1.end.z)
            }, 
            end: Vec3 {
                x: cube_1.start.x.max(cube_2.end.x).min(cube_1.end.x),
                y: cube_1.start.y.max(cube_2.end.y).min(cube_1.end.y),
                z: cube_1.start.z.max(cube_2.end.z).min(cube_1.end.z),
            }
        };
        new_cubes.push(Cube {
            start: cube_1.start.clone(),
            end: Vec3 {
                x: cube_2.start.x,
                y: cube_1.end.y,
                z: cube_1.end.z
            }
        });
        new_cubes.push(Cube {
            start: Vec3 {
                x: cube_2.end.x,
                y: cube_1.start.y,
                z: cube_1.start.z
            },
            end: cube_1.end.clone() 
        });
        new_cubes.push(Cube {
            start: Vec3 {
                x: cube_2.start.x,
                y: cube_1.start.y,
                z: cube_1.start.z
            },
            end: Vec3 {
                x: cube_2.end.x,
                y: cube_2.start.y,
                z: cube_1.end.z
            }
        });
        new_cubes.push(Cube {
            start: Vec3 {
                x: cube_2.start.x,
                y: cube_2.end.y,
                z: cube_1.start.z
            },
            end: Vec3 {
                x: cube_2.end.x,
                y: cube_1.end.y,
                z: cube_1.end.z
            }
        });
        new_cubes.push(Cube {
            start: Vec3 {
                x: cube_2.start.x,
                y: cube_2.start.y,
                z: cube_1.start.z
            },
            end: Vec3 {
                x: cube_2.end.x,
                y: cube_2.end.y,
                z: cube_2.start.z
            }
        });
        new_cubes.push(Cube {
            start: Vec3 {
                x: cube_2.start.x,
                y: cube_2.start.y,
                z: cube_2.end.z
            },
            end: Vec3 {
                x: cube_2.end.x,
                y: cube_2.end.y,
                z: cube_1.end.z
            }
        });
    }
    return new_cubes;
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("22".to_owned());

    let commands: Vec<Command> = input.iter().map(|line| {
        let command: Vec<&str> = line.trim().split(' ').collect();
        let range_str = command[1].split(',').map(|r| {
            let r = &r[2..].split("..").map(|s| s.parse().unwrap()).collect::<Vec<i128>>();
            (r[0], r[1])
        })
        .collect::<Vec<(i128, i128)>>();
        let x_range = (range_str[0].0, range_str[0].1);
        let y_range = (range_str[1].0, range_str[1].1);
        let z_range = (range_str[2].0, range_str[2].1);
        match command[0] {
            "on" => Command::On((x_range, y_range, z_range)),
            "off" => Command::Off((x_range, y_range, z_range)),
            _ => unreachable!()
        }
    })
    .collect();
    part_1(&commands);
    part_2(&commands);
}

fn part_1(commands: &Vec<Command>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut cubes = Vec::new();

    for command in commands {
        match command {
            Command::On(range) => {
                if range.0.0 >= -50 && 50 >= range.0.1 &&
                    range.1.0 >= -50 && 50 >= range.1.1 &&
                    range.2.0 >= -50 && 50 >= range.2.1 {
                    let cur_cube = Cube {
                        start: Vec3 {
                            x: range.0.0,
                            y: range.1.0,
                            z: range.2.0
                        },
                        end: Vec3 {
                            x: range.0.1 + 1,
                            y: range.1.1 + 1,
                            z: range.2.1 + 1
                        }
                    };
                    let mut new_cubes = Vec::new();
                    for other_cube in cubes.clone().iter() {
                        for child in subtract(other_cube, &cur_cube) {
                            if child.count() > 0 {
                                new_cubes.push(child);
                            }
                        }
                    }
                    cubes = new_cubes;
                    cubes.push(cur_cube);
                }
            },
            Command::Off(range) => {
                if range.0.0 >= -50 && 50 >= range.0.1 &&
                    range.1.0 >= -50 && 50 >= range.1.1 &&
                    range.2.0 >= -50 && 50 >= range.2.1 {
                    let cur_cube = Cube {
                        start: Vec3 {
                            x: range.0.0,
                            y: range.1.0,
                            z: range.2.0
                        },
                        end: Vec3 {
                            x: range.0.1 + 1,
                            y: range.1.1 + 1,
                            z: range.2.1 + 1
                        }
                    };
                    let mut new_cubes = Vec::new();
                    for other_cube in cubes.clone().iter() {
                        for child in subtract(other_cube, &cur_cube) {
                            if child.count() > 0 {
                                new_cubes.push(child);
                            }
                        }
                    }
                    cubes = new_cubes;
                }
            }
        }
    }

    let count = cubes.iter().fold(0, |acc, c| acc + c.count());
    println!("{:?}", count);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(commands: &Vec<Command>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut cubes = HashSet::new();

    for command in commands {
        match command {
            Command::On(range) => {
                let cur_cube = Cube {
                    start: Vec3 {
                        x: range.0.0,
                        y: range.1.0,
                        z: range.2.0
                    },
                    end: Vec3 {
                        x: range.0.1 + 1,
                        y: range.1.1 + 1,
                        z: range.2.1 + 1
                    }
                };
                let mut new_cubes = HashSet::new();
                for other_cube in cubes.clone().iter() {
                    for child in subtract(other_cube, &cur_cube) {
                        if child.count() > 0 {
                            new_cubes.insert(child);
                        }
                    }
                }
                cubes = new_cubes;
                cubes.insert(cur_cube);
            },
            Command::Off(range) => {
                let cur_cube = Cube {
                    start: Vec3 {
                        x: range.0.0,
                        y: range.1.0,
                        z: range.2.0
                    },
                    end: Vec3 {
                        x: range.0.1 + 1,
                        y: range.1.1 + 1,
                        z: range.2.1 + 1
                    }
                };
                let mut new_cubes = HashSet::new();
                for other_cube in cubes.clone().iter() {
                    for child in subtract(other_cube, &cur_cube) {
                        if child.count() > 0 {
                            new_cubes.insert(child);
                        }
                    }
                }
                cubes = new_cubes;
            }
        }
    }

    let count = cubes.iter().fold(0, |acc, c| acc + c.count());
    println!("{:?}", count);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
