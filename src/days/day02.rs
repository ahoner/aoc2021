use crate::etc::utils::input;
use std::time::Instant;

pub fn run() {
    let input: Vec<String> = input::fetch_input("2".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut hor = 0;
    let mut depth = 0;
    for command in input.iter() {
        let command: Vec<&str> = command.trim().split(" ").collect();
        let direction = command[0];
        let amount: i32 = command[1].parse().expect("Amount not a number");

        match direction {
            "forward" => hor += amount,
            "up" => depth -= amount,
            "down" => depth += amount,
            _ => println!("Not a valid direction")
        }
    }
    let result = hor * depth;
    println!("Time Elapsed: {}ms\nHorizontal Distance: {}\nDepth: {}\nResult: {}", part_1_time.elapsed().as_micros() as f32 / 1000.0, hor, depth, result);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut hor = 0;
    let mut depth = 0;
    let mut aim = 0;
    for command in input.iter() {
        let command: Vec<&str> = command.trim().split(" ").collect();
        let direction = command[0];
        let amount: i32 = command[1].parse().expect("Amount not a number");

        match direction {
            "forward" => {
                hor += amount;
                depth += aim * amount
            },
            "up" => aim -= amount,
            "down" => aim += amount,
            _ => println!("Not a valid direction")
        }
    }
    let result = hor * depth;
    println!("Time Elapsed: {}ms\nHorizontal Distance: {}\nDepth: {}\nResult: {}", part_2_time.elapsed().as_micros() as f32 / 1000.0, hor, depth, result);
}
