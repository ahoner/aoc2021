use crate::etc::utils::input;
use crate::etc::parser::parser::{
    Parser,
};
use std::time::{ Instant };

pub fn run() {
    let input: Vec<String> = input::fetch_input("18".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut iter = input.iter();
    let line_1 = iter.next().unwrap().trim().to_string();
    let mut depth_arr = read_snail_num(line_1);
    while let Some(line) = iter.next() {
        let line = line.trim().to_string();
        let mut depth_arr_2: Vec<(u32,u32)> = read_snail_num(line);
        for i in 0..depth_arr.len() {
            depth_arr[i].1 += 1;
        }
        for i in 0..depth_arr_2.len() {
            depth_arr_2[i].1 += 1;
        }
        depth_arr.append(&mut depth_arr_2);
        while explode(&mut depth_arr) || split(&mut depth_arr) {}
    }
    while depth_arr.len() > 1 {
        'reduce: for i in 0..depth_arr.len() {
            if depth_arr[i].1 == depth_arr[i + 1].1 {
                depth_arr[i].0 = depth_arr[i].0 * 3 + depth_arr[i + 1].0 * 2;
                depth_arr.remove(i + 1);
                depth_arr[i].1 -= 1;
                break 'reduce;
            }
        }
    }
    println!("{:?}", depth_arr);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut max_sum = 0;
    for line1 in input {
        for line2 in input {
            if line1 != line2 {
                let line_1 = line1.trim().to_string();
                let line_2 = line2.trim().to_string();
                let mut depth_arr = read_snail_num(line_1);
                let mut depth_arr_2: Vec<(u32,u32)> = read_snail_num(line_2);
                for i in 0..depth_arr.len() {
                    depth_arr[i].1 += 1;
                }
                for i in 0..depth_arr_2.len() {
                    depth_arr_2[i].1 += 1;
                }
                depth_arr.append(&mut depth_arr_2);
                while explode(&mut depth_arr) || split(&mut depth_arr) {}
                while depth_arr.len() > 1 {
                    'reduce: for i in 0..depth_arr.len() {
                        if depth_arr[i].1 == depth_arr[i + 1].1 {
                            depth_arr[i].0 = depth_arr[i].0 * 3 + depth_arr[i + 1].0 * 2;
                            depth_arr.remove(i + 1);
                            depth_arr[i].1 -= 1;
                            break 'reduce;
                        }
                    }
                }
                if depth_arr[0].0 > max_sum {
                    max_sum = depth_arr[0].0;
                }
            }
        }
    }

    println!("Max Sum: {}", max_sum);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn read_snail_num(arr: String) -> Vec<(u32, u32)> {
    let parse_number = match_number();
    let mut depth = 0;
    let mut ret = Vec::new();
    let mut i = 0;
    while i < arr.len() {
        let c = arr.chars().nth(i).unwrap();
        match c {
            '[' => depth += 1,
            ']' => depth -= 1,
            ',' => (),
            _ => {
                let (rest, n) = parse_number.parse(&arr[i..]).unwrap();
                ret.push((n, depth));
                i += arr[i..].len() - rest.len() - 1
            }
        }
        i += 1;
    }
    return ret;
}

fn match_number<'a>() -> impl Parser<'a, u32> {
    move |input: &'a str| {
        let mut result = String::new();
        let mut i = 1;
        while &input[(i - 1)..i] != "[" && &input[(i - 1)..i] != "]" && &input[(i - 1)..i] != "," {
            result = input[0..i].to_string();
            i += 1;
        }
        if result == "" {
            return Err(input);
        }
        return Ok((&input[result.len()..], result.parse().unwrap()))
    }
}

fn explode(arr: &mut Vec<(u32, u32)>) -> bool {
    for i in 0..arr.len() {
        let (value, depth) = arr[i];
        if depth > 4 {
            if i > 0 {
                arr[i - 1].0 += value;
            }
            if arr.len() - 2 > i {
                arr[i + 2].0 += arr[i + 1].0
            }
            arr.splice(i..i + 2, vec![(0, depth - 1)]);
            return true;
        }
    }
    return false;
}

fn split(arr: &mut Vec<(u32, u32)>) -> bool {
    for i in 0..arr.len() {
        let (value, depth) = arr[i];
        if value >= 10 {
            arr.splice(i..i+1, vec![(value / 2, depth + 1), ((0..value).step_by(2).size_hint().0 as u32, depth + 1)]);
            return true;
        }
    }
    return false;
}
