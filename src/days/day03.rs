use crate::etc::utils::input;
use std::time::Instant;

pub fn run() {
    let input: Vec<String> = input::fetch_input("3".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let bin_length = input[0].len();
    let gamma = input.into_iter()
        .map(|line| usize::from_str_radix(line, 2).unwrap())
        .fold(vec![0; bin_length], |count, bits| {
            count.into_iter()
                .enumerate()
                .map(|(i, b)| b + ((bits & 1 << i) >> i))
                .collect()
        })
        .into_iter()
        .enumerate()
        .map(|(i, b)| ((b >= input.len() / 2) as u32) << i)
        .sum::<u32>();
    let epsilon = !gamma & ((1 << bin_length) - 1);
    println!("{} * {} = {}", gamma, epsilon, gamma * epsilon);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let bin_length = input[0].len();
    let part_2_time = Instant::now();
    let input: Vec<usize> = input.into_iter()
        .map(|line| usize::from_str_radix(line, 2).unwrap())
        .collect();
    
    let ox = (0..bin_length).rev()
        .scan(input.clone(), |ox, i| {
            let digit_sum = ox.iter()
                .filter(|n| *n & 1 << i > 0)
                .count();
            let is_one_most_common = digit_sum >= (ox.len() + 1) / 2;
            ox.drain_filter(|n| (*n & 1 << i > 0) != is_one_most_common);
            ox.first().copied()
        })
        .last()
        .unwrap();

    let co2 = (0..bin_length).rev()
        .scan(input, |co2, i| {
            let digit_sum = co2.iter()
                .filter(|n| *n & 1 << i > 0)
                .count();
            let is_one_most_common = digit_sum >= (co2.len() + 1) / 2;
            co2.drain_filter(|n| (*n & 1 << i > 0) == is_one_most_common);
            co2.first().copied()
        })
        .last()
        .unwrap();

    println!("{} * {} = {}", ox, co2, ox * co2); 
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
