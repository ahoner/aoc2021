use crate::etc::utils::input;
use std::time::{ Duration, Instant };

use sdl2::pixels::Color;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::render::WindowCanvas;
use sdl2::rect::Rect;

#[derive(Debug)]
enum Octo {
    Flashed,
    Charging(u32)
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("11".to_owned());
    part_1(&input);
    part_2(&input);
    animation(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut octos: Vec<Vec<Octo>> = input.iter()
        .map(|line| line.chars()
            .map(|c| Octo::Charging(c.to_digit(10).unwrap()))
            .collect::<Vec<Octo>>()
        )
        .collect();

    let mut flashes = 0;
    
    for _i in 0..100 {
        inc_octos(&mut octos);
        check_flashes(&mut octos, &mut flashes);
        reset_flashed(&mut octos);
    }

    println!("flashes: {}", flashes);
    
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let mut octos: Vec<Vec<Octo>> = input.iter()
        .map(|line| line.chars()
            .map(|c| Octo::Charging(c.to_digit(10).unwrap()))
            .collect::<Vec<Octo>>()
        )
        .collect();

    let mut flashes = 0;
    let mut i = 1;
    
    'flash_loop: loop {
        let curr_flashes = flashes;
        inc_octos(&mut octos);
        check_flashes(&mut octos, &mut flashes);
        if flashes - curr_flashes == 100 {
            break 'flash_loop;
        }
        reset_flashed(&mut octos);
        i += 1;
    }

    println!("First Sync: {}", i);
    
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn animation(input: &Vec<String>) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("day 11", 800, 800)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut octos: Vec<Vec<Octo>> = input.iter()
        .map(|line| line.chars()
            .map(|c| Octo::Charging(c.to_digit(10).unwrap()))
            .collect::<Vec<Octo>>()
        )
        .collect();

    let mut flashes = 0;

    'game: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'game;
                },
                _ => {}
            }
        }
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        inc_octos(&mut octos);
        check_flashes(&mut octos, &mut flashes);
        draw_octos(&octos, &mut canvas);
        reset_flashed(&mut octos);

        canvas.present();
        ::std::thread::sleep(Duration::new(0, 40_000_000u32));
    }
}


fn inc_octos(octos: &mut Vec<Vec<Octo>>) {
    for y in 0..octos.len() {
        for x in 0..octos[y].len() {
            if let Octo::Charging(v) = octos[y][x] {
                octos[y][x] = Octo::Charging(v + 1);
            }
        }
    }
}

fn check_flashes(octos: &mut Vec<Vec<Octo>>, flashes: &mut u32) {
    for y in 0..octos.len() {
        for x in 0..octos[y].len() {
            if let Octo::Charging(v) = octos[y][x] {
                if v > 9 {
                    *flashes += 1;
                    flash(octos, x, y, flashes);
                }
            }
        }
    }
}

fn flash(octos: &mut Vec<Vec<Octo>>, x: usize, y: usize, flashes: &mut u32) {
    octos[y][x] = Octo::Flashed;
    if y > 0 {
        if x > 0 {
            if let Octo::Charging(v) = octos[y - 1][x - 1] {
                if v + 1 > 9 {
                    *flashes += 1;
                    flash(octos, x - 1, y - 1, flashes);
                } else {
                    octos[y - 1][x - 1] = Octo::Charging(v + 1);
                }
            }
        }
        if octos[y - 1].len() - 1 > x {
            if let Octo::Charging(v) = octos[y - 1][x + 1] {
                if v + 1 > 9 {
                    *flashes += 1;
                    flash(octos, x + 1, y - 1, flashes);
                } else {
                    octos[y - 1][x + 1] = Octo::Charging(v + 1);
                }
            }
        }
        if let Octo::Charging(v) = octos[y - 1][x] {
            if v + 1 > 9 {
                *flashes += 1;
                flash(octos, x, y - 1, flashes);
            } else {
                octos[y - 1][x] = Octo::Charging(v + 1);
            }
        }
    }
    if octos.len() - 1 > y {
        if x > 0 {
            if let Octo::Charging(v) = octos[y + 1][x - 1] {
                if v + 1 > 9 {
                    *flashes += 1;
                    flash(octos, x - 1, y + 1, flashes);
                } else {
                    octos[y + 1][x - 1] = Octo::Charging(v + 1);
                }
            }
        }
        if octos[y + 1].len() - 1 > x {
            if let Octo::Charging(v) = octos[y + 1][x + 1] {
                if v + 1 > 9 {
                    *flashes += 1;
                    flash(octos, x + 1, y + 1, flashes);
                } else {
                    octos[y + 1][x + 1] = Octo::Charging(v + 1);
                }
            }
        }
        if let Octo::Charging(v) = octos[y + 1][x] {
            if v + 1 > 9 {
                *flashes += 1;
                flash(octos, x, y + 1, flashes);
            } else {
                octos[y + 1][x] = Octo::Charging(v + 1);
            }
        }
    }
    if x > 0 {
        if let Octo::Charging(v) = octos[y][x - 1] {
            if v + 1 > 9 {
                *flashes += 1;
                flash(octos, x - 1, y, flashes);
            } else {
                octos[y][x - 1] = Octo::Charging(v + 1);
            }
        }
    }
    if octos[y].len() - 1 > x {
        if let Octo::Charging(v) = octos[y][x + 1] {
            if v + 1 > 9 {
                *flashes += 1;
                flash(octos, x + 1, y, flashes);
            } else {
                octos[y][x + 1] = Octo::Charging(v + 1);
            }
        }
    }
}

fn reset_flashed(octos: &mut Vec<Vec<Octo>>) {
    for y in 0..octos.len() {
        for x in 0..octos[y].len() {
            if let Octo::Flashed = octos[y][x] {
                octos[y][x] = Octo::Charging(0);
            }
        }
    }
}

fn draw_octos(octos: &Vec<Vec<Octo>>, canvas: &mut WindowCanvas) {
    for y in 0..octos.len() {
        for x in 0..octos[y].len() {
            let rect = Rect::new((x * 80) as i32, (y * 80) as i32, 80, 80);
            match octos[y][x] {
                Octo::Charging(v) => {
                    canvas.set_draw_color(Color::RGB(0, ((255 * v) / (9 * 4)) as u8, 0));
                    canvas.fill_rect(rect).expect("oh no gwaphics UwU");
                },
                Octo::Flashed => {
                    canvas.set_draw_color(Color::RGB(0, 255, 0));
                    canvas.fill_rect(rect).expect("oh no gwaphics UwU");
                }
            }
        }
    }
}
