
use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::HashMap;

pub fn run() {
    let input: Vec<String> = input::fetch_input("20".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let input_str: String = input.join("\n");
    let input_arr: Vec<String> = input_str.split("\n\n").map(|s| s.to_string()).collect();
    let ie_lookup: Vec<_> = Vec::from(input_arr[0].clone());
    let mut input_image = HashMap::new();
    let mut range: (isize, isize, isize, isize) = (0, 0, 0, 0);
    for (y, l) in input_arr[1].split("\n").enumerate() {
        for (x, c) in l.chars().enumerate() {
            input_image.insert((x as isize, y as isize), c);
            range.2 = range.2.max(x as isize);
            range.3 = range.3.max(y as isize);
        }
    }

    print_image(&input_image, range);
    let loops = 50;
    let mut outside_val = '0';
    for _i in 0..loops {
        range = add_border(&mut input_image, range, outside_val);
        input_image = enchance(&input_image, &ie_lookup, outside_val);
        if outside_val == '0' {
            outside_val = if ie_lookup[0] == 46 { '0' } else { '1' };
        } else {
            outside_val = if ie_lookup[ie_lookup.len() - 1] == 46 { '0' } else { '1' };
        }
    }
    print_image(&input_image, range);
    println!("{}", input_image.values().fold(0, |acc, v| {
        if *v == '#' {
            acc + 1
        } else {
            acc
        }
    }));
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(_input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn enchance(input_image: &HashMap<(isize, isize), char>,
    ie_lookup: &Vec<u8>,
    outside_val: char
) -> HashMap<(isize, isize), char> {
    let mut output_image = HashMap::new();
    for (x, y) in input_image.keys() {
        let mut lookup_str = String::new();
        for j in -1..=1 {
            for i in -1..=1 {
                match input_image.get(&(x + i, y + j)) {
                    Some(c) => {
                        if *c == '#' {
                            lookup_str.push('1');
                        } else {
                            lookup_str.push('0');
                        }
                    }
                    None => {
                        lookup_str.push(outside_val);
                    }
                }
            }
        }
        let i = usize::from_str_radix(&lookup_str, 2).unwrap();
        match ie_lookup[i] {
            46 => {
                output_image.insert((*x, *y), '.');
            },
            35 => {
                output_image.insert((*x, *y), '#');
            },
            _ => {
                unreachable!();
            }
        }
    }
    output_image
}

fn add_border(image: &mut HashMap<(isize, isize), char>,
    (x_min, y_min, x_max, y_max): (isize, isize, isize, isize),
    outside_val: char
) -> (isize, isize, isize, isize) {
    let pixel_char = if outside_val == '0' { '.' } else { '#' };
    for y in (y_min - 1)..=(y_max + 1) {
        image.insert((x_min - 1, y), pixel_char);
        image.insert((x_max + 1, y), pixel_char);
    }
    for x in (x_min - 1)..=(x_max + 1) {
        image.insert((x, y_min - 1), pixel_char);
        image.insert((x, y_max + 1), pixel_char);
    }
    return (x_min - 1, y_min - 1, x_max + 1, y_max + 1);
}

fn print_image(image: &HashMap<(isize, isize), char>,
    (x_min, y_min, x_max, y_max): (isize, isize, isize, isize)) {
    for y in y_min..=y_max {
        for x in x_min..=x_max {
            print!("{}", image.get(&(x, y)).unwrap());
        }
        println!("");
    }
    println!("");
}