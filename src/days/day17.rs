use crate::etc::utils::input;
use std::time::{ Instant };

pub fn run() {
    let input: Vec<String> = input::fetch_input("17".to_owned());
    part_1_and_2(&input);
}

fn part_1_and_2(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let tmp = &input[0][13..].split(", ").collect::<Vec<&str>>().iter().map(|s| {
        let tmp = s[2..].split("..").collect::<Vec<&str>>();
        (tmp[0].parse().unwrap(), tmp[1].parse().unwrap())
    }).collect::<Vec<(i32, i32)>>();
    let (x_min, x_max) = tmp[0];
    let (y_min, y_max) = tmp[1];

    let mut max = (0, (0, 0));
    let mut hits = 0;

    for x in 0..1000 {
        for y in -1000..1000 {
            let mut forces = (x, y);
            let mut point = (0, 0);
            let mut max_y = 0;
            let current_hit_count = hits;
            'sim_loop: loop {
                point = (point.0 + forces.0, point.1 + forces.1);
                if forces.0 > 0 {
                    forces.0 -= 1;
                } else if 0 > forces.0 {
                    forces.0 += 1;
                }
                forces.1 -= 1;
                match is_hit(((x_min, x_max), (y_min, y_max)), point) {
                    ProbeStatus::Hit => {
                        hits += 1;
                        break 'sim_loop;
                    }
                    ProbeStatus::Miss => break 'sim_loop,
                    ProbeStatus::NotYet => {
                        if point.1 > max_y {
                            max_y = point.1
                        }
                    }
                }
            }
            if max_y > max.0 && hits > current_hit_count {
                max = (max_y, point);
            }
        }
    }

    println!("x_min: {}, x_max: {}, y_min: {}, y_max: {}", x_min, x_max, y_min, y_max);
    println!("{:?}", max);
    println!("{:?}", hits);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

#[derive(Debug)]
enum ProbeStatus {
    Hit,
    Miss,
    NotYet
}

fn is_hit(target: ((i32, i32), (i32, i32)), point: (i32, i32)) -> ProbeStatus {
    let ((x_min, x_max), (y_min, y_max)) = target;
    let (x, y) = point;
    if x >= x_min && x_max >= x && y >= y_min && y_max >= y {
        return ProbeStatus::Hit
    } else if x > x_max || y_min > y {
        return ProbeStatus::Miss
    } else {
        return ProbeStatus::NotYet
    }
}
