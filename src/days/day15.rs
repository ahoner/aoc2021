use crate::etc::utils::input;
use std::time::{ Instant };
use std::cmp::Ordering;
use std::collections::{ HashMap, HashSet };

const NEIGHBOR_TRANSFORMS: [(isize, isize); 4] = [(1, 0), (0, 1), (-1, 0), (0, -1)];

#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    point: (isize, isize),
    val: u32
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other.val.cmp(&self.val)
            .then_with(|| self.point.cmp(&other.point))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("15".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut map: HashMap<(isize, isize), u32> = HashMap::new();
    let mut x_max = 0;
    let mut y_max = 0;
    for (y, line) in input.iter().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert((x as isize, y as isize), c.to_digit(10).unwrap());
            if x > x_max {
                x_max = x;
            }
        }
        if y > y_max {
            y_max = y;
        }
    }

    let mut open_set: HashSet<(isize, isize)> = HashSet::new();
    let mut came_from: HashMap<(isize, isize), (isize, isize)> = HashMap::new();
    let mut g_score: HashMap<(isize, isize), u32> = HashMap::new();
    let mut f_score: HashMap<(isize, isize), u32> = HashMap::new();
    open_set.insert((0, 0));
    g_score.insert((0, 0), 0);
    f_score.insert((0, 0), (x_max + y_max) as u32);

    'a_star: while open_set.len() > 0 {
        let mut current: Option<(isize, isize)> = None;
        let mut current_score = u32::MAX;
        for point in open_set.iter() {
            if let Some(val) = f_score.get(point) {
                if current_score > *val {
                    current = Some(point.clone());
                    current_score = *val
                }
            }
        }
        if current.unwrap() == (x_max as isize, y_max as isize) {
            break 'a_star;
        }
        open_set.remove(&current.unwrap());
        let current = current.unwrap();
        let mut neighbors: Vec<(isize, isize)> = vec![];
        for (d_x, d_y) in NEIGHBOR_TRANSFORMS {
            if map.contains_key(&(current.0 + d_x, current.1 + d_y)) {
                neighbors.push((current.0 + d_x, current.1 + d_y));
            }
        }
        for neighbor in neighbors {
            let tentative_g_score = g_score.get(&current).unwrap() + map.get(&neighbor).unwrap();
            if let Some(score) = g_score.get(&neighbor) {
                if score > &tentative_g_score {
                    came_from.insert(neighbor, current);
                    g_score.insert(neighbor, tentative_g_score);
                    f_score.insert(neighbor, (tentative_g_score + (x_max - neighbor.0 as usize) as u32) + (y_max - neighbor.0 as usize) as u32);
                    if !open_set.contains(&neighbor) {
                        open_set.insert(neighbor);
                    }
                }
            } else {
                if u32::MAX > tentative_g_score {
                    came_from.insert(neighbor, current);
                    g_score.insert(neighbor, tentative_g_score);
                    f_score.insert(neighbor, (tentative_g_score + (x_max - neighbor.0 as usize) as u32) + (y_max - neighbor.0 as usize) as u32);
                    if !open_set.contains(&neighbor) {
                        open_set.insert(neighbor);
                    }
                }
            }
        }
    }

    let mut path: Vec<(isize, isize)> = vec![(x_max as isize, y_max as isize)];
    let mut current = (x_max as isize, y_max as isize);
    while current != (0, 0) {
        current = *came_from.get(&current).unwrap();
        path.insert(0, current);
    }

    let risk = path.iter().fold(0, |acc, p| {
        acc + map.get(p).unwrap()
    });
    println!("{:?}", risk - map.get(&(0, 0)).unwrap());

    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let mut map: HashMap<(isize, isize), u32> = HashMap::new();
    let mut x_max = 0;
    let mut y_max = 0;
    for (y, line) in input.iter().enumerate() {
        for (x, c) in line.chars().enumerate() {
            map.insert((x as isize, y as isize), c.to_digit(10).unwrap());
            if x > x_max {
                x_max = x;
            }
        }
        if y > y_max {
            y_max = y;
        }
    }
    for ((x, y), val) in map.clone().iter() { 
        for i in 0..5 {
            for j in 0..5 {
                let calc_x = x + (j * (x_max + 1)) as isize;
                let calc_y = y + (i * (y_max + 1)) as isize;
                let calc_val = (((*val + i as u32 + j as u32) - 1) % 9) + 1;
                map.insert((calc_x, calc_y), calc_val);
            }
        }
    }

    x_max = 5 * x_max + 4;
    y_max = 5 * y_max + 4;

    let mut open_set: HashSet<(isize, isize)> = HashSet::new();
    let mut came_from: HashMap<(isize, isize), (isize, isize)> = HashMap::new();
    let mut g_score: HashMap<(isize, isize), u32> = HashMap::new();
    let mut f_score: HashMap<(isize, isize), u32> = HashMap::new();
    open_set.insert((0, 0));
    g_score.insert((0, 0), 0);
    f_score.insert((0, 0), (x_max + y_max) as u32);

    'a_star: while open_set.len() > 0 {
        let mut current: Option<(isize, isize)> = None;
        let mut current_score = u32::MAX;
        for point in open_set.iter() {
            if let Some(val) = f_score.get(point) {
                if current_score > *val {
                    current = Some(point.clone());
                    current_score = *val
                }
            } else {
                if current_score == u32::MAX {
                    current = Some(point.clone());
                }
            }
        }
        if current.unwrap() == (x_max as isize, y_max as isize) {
            println!("oh shit waddup");
            break 'a_star;
        }
        open_set.remove(&current.unwrap());
        let current = current.unwrap();
        let mut neighbors: Vec<(isize, isize)> = vec![];
        for (d_x, d_y) in NEIGHBOR_TRANSFORMS {
            if map.contains_key(&(current.0 + d_x, current.1 + d_y)) {
                neighbors.push((current.0 + d_x, current.1 + d_y));
            }
        }
        for neighbor in neighbors {
            let tentative_g_score = g_score.get(&current).unwrap() + map.get(&neighbor).unwrap();
            if let Some(score) = g_score.get(&neighbor) {
                if score > &tentative_g_score {
                    came_from.insert(neighbor, current);
                    g_score.insert(neighbor, tentative_g_score);
                    f_score.insert(neighbor, (tentative_g_score + (x_max - neighbor.0 as usize) as u32) + (y_max - neighbor.0 as usize) as u32);
                    if !open_set.contains(&neighbor) {
                        open_set.insert(neighbor);
                    }
                }
            } else {
                if u32::MAX > tentative_g_score {
                    came_from.insert(neighbor, current);
                    g_score.insert(neighbor, tentative_g_score);
                    f_score.insert(neighbor, (tentative_g_score + (x_max - neighbor.0 as usize) as u32) + (y_max - neighbor.0 as usize) as u32);
                    if !open_set.contains(&neighbor) {
                        open_set.insert(neighbor);
                    }
                }
            }
        }
    }

    let mut path: Vec<(isize, isize)> = vec![(x_max as isize, y_max as isize)];
    let mut current = (x_max as isize, y_max as isize);
    while current != (0, 0) {
        current = *came_from.get(&current).unwrap();
        path.insert(0, current);
    }

    let risk = path.iter().fold(0, |acc, p| {
        acc + map.get(p).unwrap()
    });
    println!("{:?}", risk - map.get(&(0, 0)).unwrap());

    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
