use crate::etc::utils::input;
use std::collections::HashSet;
use std::cmp::Ordering::{Less, Equal, Greater};
use sdl2::pixels::Color;
use sdl2::rect::Point as Sdl2Point;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use std::time::{ Duration, Instant };

pub fn run() {
    let input: Vec<String> = input::fetch_input("5".to_owned());
    part_1(&input);
    part_2(&input);
    animation(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let vents: Vec<Line> = input.iter()
        .map(|line| {
            let points: Vec<Point> = line.trim().split(" -> ")
                .map(|p| {
                    let xy: Vec<i32> = p.split(",")
                        .map(|n| n.parse().expect("Not a number"))
                        .collect();
                    Point {
                        x: xy[0],
                        y: xy[1]
                    }
                })
                .collect();
            if points[0].x == points[1].x {
                return Line {
                    start: points[0],
                    end: points[1],
                    line_type: LineType::Horizontal
                }
            } else if points[0].y == points[1].y {
                return Line {
                    start: points[0],
                    end: points[1],
                    line_type: LineType::Vertical
                }
            } else {
                return Line {
                    start: points[0],
                    end: points[1],
                    line_type: LineType::Diagonal
                }
            }
        })
        .collect();
    let mut mapped_vents: HashSet<Point> = HashSet::new();
    let mut collision_set: HashSet<Point> = HashSet::new();
    for line in vents {
        match line.line_type {
            LineType::Horizontal |
            LineType::Vertical => {
                let mut x_offset = 0;
                let mut y_offset = 0;
                let d_x = match line.start.x.cmp(&line.end.x) {
                    Less => 1,
                    Greater => -1,
                    Equal => 0,
                };
                let d_y = match line.start.y.cmp(&line.end.y) {
                    Less => 1,
                    Greater => -1,
                    Equal => 0,
                };
                'create_line: loop {
                    if line.start.x + x_offset == line.end.x && line.start.y + y_offset == line.end.y {
                        break 'create_line;
                    }
                    if !mapped_vents.insert(Point {
                        x: line.start.x + x_offset,
                        y: line.start.y + y_offset
                    }) {
                        collision_set.insert(Point {
                            x: line.start.x + x_offset,
                            y: line.start.y + y_offset
                        });
                    }
                    x_offset += d_x;
                    y_offset += d_y;
                }
                if !mapped_vents.insert(Point {
                    x: line.start.x + x_offset,
                    y: line.start.y + y_offset
                }) {
                    collision_set.insert(Point {
                        x: line.start.x + x_offset,
                        y: line.start.y + y_offset
                    });
                }
            },
            LineType::Diagonal => ()
        }
    }
    println!("{:?}", collision_set.len());
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let vents: Vec<Line> = input.iter()
        .map(|line| {
            let points: Vec<Point> = line.trim().split(" -> ")
                .map(|p| {
                    let xy: Vec<i32> = p.split(",")
                        .map(|n| n.parse().expect("Not a number"))
                        .collect();
                    Point {
                        x: xy[0],
                        y: xy[1]
                    }
                })
                .collect();
            if points[0].x == points[1].x {
                return Line {
                    start: points[0],
                    end: points[1],
                    line_type: LineType::Horizontal
                }
            } else if points[0].y == points[1].y {
                return Line {
                    start: points[0],
                    end: points[1],
                    line_type: LineType::Vertical
                }
            } else {
                return Line {
                    start: points[0],
                    end: points[1],
                    line_type: LineType::Diagonal
                }
            }
        })
        .collect();
    let mut mapped_vents: HashSet<Point> = HashSet::new();
    let mut collision_set: HashSet<Point> = HashSet::new();
    for line in vents {
        let mut x_offset = 0;
        let mut y_offset = 0;
        let d_x = match line.start.x.cmp(&line.end.x) {
            Less => 1,
            Greater => -1,
            Equal => 0,
        };
        let d_y = match line.start.y.cmp(&line.end.y) {
            Less => 1,
            Greater => -1,
            Equal => 0,
        };
        'create_line: loop {
            if line.start.x + x_offset == line.end.x && line.start.y + y_offset == line.end.y {
                break 'create_line;
            }
            if !mapped_vents.insert(Point {
                x: line.start.x + x_offset,
                y: line.start.y + y_offset
            }) {
                collision_set.insert(Point {
                    x: line.start.x + x_offset,
                    y: line.start.y + y_offset
                });
            }
            x_offset += d_x;
            y_offset += d_y;
        }
        if !mapped_vents.insert(Point {
            x: line.start.x + x_offset,
            y: line.start.y + y_offset
        }) {
            collision_set.insert(Point {
                x: line.start.x + x_offset,
                y: line.start.y + y_offset
            });
        }
    }
    println!("{:?}", collision_set.len());
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn animation(input: &Vec<String>) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("day 5", 1000, 1000)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut input_iter = input.iter();
    'game: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'game;
                },
                _ => {}
            }
        }
        // RENDER STUFF
        if let Some(line) = input_iter.next() {
            let points: Vec<Point> = line.trim().split(" -> ")
                .map(|p| {
                    let xy: Vec<i32> = p.split(",")
                        .map(|n| n.parse().expect("Not a number"))
                        .collect();
                    Point {
                        x: xy[0],
                        y: xy[1]
                    }
                })
                .collect();
            let start = Sdl2Point::new(points[0].x, points[0].y);
            let end = Sdl2Point::new(points[1].x, points[1].y);
            canvas.set_draw_color(Color::RGB(255, 255, 255));
            canvas.draw_line(start, end).expect("Draw error");
            // STOP RENDER STUFF

            canvas.present();
            ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, std::hash::Hash)]
struct Point {
    x: i32,
    y: i32
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool  {
        self.x == other.x && self.y == other.y
    }
}

#[derive(Debug)]
struct Line {
    start: Point,
    end: Point,
    line_type: LineType
}

#[derive(Debug)]
enum LineType {
    Vertical,
    Horizontal,
    Diagonal
}
