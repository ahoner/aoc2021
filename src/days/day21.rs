use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::HashMap;

#[derive(Eq, Hash, PartialEq)]
struct BoardState {
    p_1_score: u8,
    p_1_pos: u8,
    p_2_score: u8,
    p_2_pos: u8,
    is_p1_turn: bool
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("21".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let mut p_1_pos = input[0].trim().chars().last().unwrap().to_digit(10).unwrap();
    let mut p_2_pos = input[1].trim().chars().last().unwrap().to_digit(10).unwrap();
    let mut p_1_score = 0;
    let mut p_2_score = 0;
    let mut roll = 6;
    let mut roll_count = 0;
    let mut is_p1_turn = true;

    while 1000 > p_1_score {
        if is_p1_turn {
            p_1_pos = ((p_1_pos + roll - 1) % 10) + 1;
            p_1_score += p_1_pos;
            is_p1_turn = false;
        } else {
            p_2_pos = ((p_2_pos + roll - 1) % 10) + 1;
            p_2_score += p_2_pos;
            is_p1_turn = true;
        }
        roll = ((roll + 8) % 100) + 1;
        roll_count += 3;
    } 

    println!("Player 1 score: {}, Player 2 score: {}", p_1_score, p_2_score);
    println!("Roll: {}", roll_count);
    println!("Solution: {}", roll_count * p_2_score);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let initial_state = BoardState {
        p_1_score: 0,
        p_1_pos: input[0].trim().chars().last().unwrap().to_digit(10).unwrap() as u8,
        p_2_score: 0,
        p_2_pos: input[1].trim().chars().last().unwrap().to_digit(10).unwrap() as u8,
        is_p1_turn: true
    };
    let mut result_lookup = HashMap::new();

    let wins = compute_game_result(initial_state, &mut result_lookup);

    println!("{:?}", wins.0.max(wins.1));
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn compute_game_result(board_state: BoardState, result_lookup: &mut HashMap<BoardState, (u64, u64)>) -> (u64, u64) {
    if let Some(result) = result_lookup.get(&board_state) {
        return *result;
    }
    if board_state.p_1_score >= 21 {
        result_lookup.insert(board_state, (1, 0));
        return (1, 0);
    } else if board_state.p_2_score >= 21 {
        result_lookup.insert(board_state, (0, 1));
        return (0, 1);
    }
    let mut p_1_wins = 0;
    let mut p_2_wins = 0;
    for i in 1..=3 {
        for j in 1..=3 {
            for k in 1..=3 {
                if board_state.is_p1_turn {
                    let p_1_pos = ((board_state.p_1_pos + i + j + k - 1) % 10) + 1;
                    let result = compute_game_result(BoardState{
                        p_1_score: board_state.p_1_score + p_1_pos,
                        p_1_pos: p_1_pos,
                        p_2_score: board_state.p_2_score,
                        p_2_pos: board_state.p_2_pos,
                        is_p1_turn: !board_state.is_p1_turn
                    }, result_lookup);
                    p_1_wins += result.0;
                    p_2_wins += result.1;
                } else {
                    let p_2_pos = ((board_state.p_2_pos + i + j + k - 1) % 10) + 1;
                    let result = compute_game_result(BoardState{
                        p_1_score: board_state.p_1_score,
                        p_1_pos: board_state.p_1_pos,
                        p_2_score: board_state.p_2_score + p_2_pos,
                        p_2_pos: p_2_pos,
                        is_p1_turn: !board_state.is_p1_turn
                    }, result_lookup);
                    p_1_wins += result.0;
                    p_2_wins += result.1;
                }
            }
        }
    }
    result_lookup.insert(board_state, (p_1_wins, p_2_wins));
    return (p_1_wins, p_2_wins);
}