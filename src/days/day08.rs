use crate::etc::utils::input;
use std::time::{ Instant };

pub fn run() {
    let input: Vec<String> = input::fetch_input("8".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let easy_digits: usize = input.iter()
        .fold(0, |acc, l| {
            let output = l.trim().split("|").collect::<Vec<&str>>()[1];
            acc + output.trim().split(" ")
                .fold(0, |acc, s| {
                    match s.len() {
                        2 | 4 | 3 | 7 => acc + 1,
                        _ => acc
                    }
                } )
        });
    println!("{:?}", easy_digits);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let sum = input.iter()
        .fold(0, |acc, l| {
            let mut iter = l.trim().split("|").into_iter();
            let mangled_input: Vec<&str> = iter.next().unwrap().trim().split(" ").collect();
            let mangled_output: Vec<&str> = iter.next().unwrap().trim().split(" ").collect();
            // Create Axioms
            let mut one = 0b00000000u8;
            let mut four: u8 = 0b00000000u8;
            let mut seven: u8 = 0b00000000u8;
            let mut eight = 0b00000000u8;
            for s in mangled_input {
                match s.len() {
                    2 => one = parse_bin(s),
                    3 => seven = parse_bin(s),
                    4 => four = parse_bin(s),
                    7 => eight = parse_bin(s),
                    _ => ()
                }
            }
            acc + mangled_output.iter().enumerate()
                .fold(0, |acc, (i, s)| {
                    let binary = parse_bin(s);
                    let num = match s.len() {
                        2 => 1,
                        3 => 7,
                        4 => 4,
                        5 => {
                            if (binary | four) == eight {
                                2
                            } else if ((((four | seven) ^ eight) | one) | binary) == eight{
                                5
                            } else {
                                3
                            }
                        },
                        6 => {
                            if ((one ^ four) | binary) == eight {
                                0
                            } else if (one | binary) == eight {
                                6
                            } else {
                                9
                            }
                        },
                        7 => 8,
                        // Should never get here with valid input....
                        _ => 0
                    };
                    acc + num * u32::pow(10, (mangled_output.len() - i) as u32)
                })
        });
    println!("{}", sum);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn parse_bin(input: &str) -> u8 {
    let mut ret_val: u8 = 0b000000000u8;
    for c in input.chars() {
        match c {
            'a' => ret_val += 0b01000000u8,
            'b' => ret_val += 0b00100000u8,
            'c' => ret_val += 0b00010000u8,
            'd' => ret_val += 0b00001000u8,
            'e' => ret_val += 0b00000100u8,
            'f' => ret_val += 0b00000010u8,
            'g' => ret_val += 0b00000001u8,
            // Should never get here with valid input....
            _ => ret_val += 0b00000000u8
        }
    }
    return ret_val;
}
