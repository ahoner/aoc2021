use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::{ HashMap, HashSet };

#[derive(Debug)]
struct Node {
    val: String,
    children: Vec<u32>
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.val == other.val
    }
}

#[derive(Debug)]
struct Tree {
    root: Option<u32>,
    arena: Vec<Option<Node>>
}

impl Tree {
    pub fn new() -> Self {
        Tree {
            arena: Vec::new(),
            root: None
        }
    }

    pub fn set_root(&mut self, root: Option<u32>) {
        self.root = root;
    }

    pub fn add_node(&mut self, parent: Option<u32>, node: Node) -> u32 {
        let i = self.arena.len() as u32;
        self.arena.push(Some(node));
        if parent == None {
            return 0;
        }
        let parent_node = self.node_at_mut(parent.unwrap()).unwrap();
        parent_node.children.push(i);
        return i;
    }

    pub fn val_at(&mut self, index: u32) -> Option<String> {
        return if let Some(node) = self.arena.get_mut(index as usize) {
            Some(node.as_mut().unwrap().val.to_string())
        } else {
            None
        }
    }

    pub fn node_at(&self, index: u32) -> Option<&Node> {
        return if let Some(node) = self.arena.get(index as usize) {
            node.as_ref()
        } else {
            None
        }
    }

    pub fn node_at_mut(&mut self, index: u32) -> Option<&mut Node> {
        return if let Some(node) = self.arena.get_mut(index as usize) {
            node.as_mut()
        } else {
            None
        }
    }
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("12".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut map: HashMap<String, Vec<String>> = HashMap::new();
    for line in input {
        let edge: Vec<&str> = line.trim().split("-").collect();
        if map.contains_key(edge[0]) {
            map.get_mut(edge[0]).unwrap().push(edge[1].to_string());
        } else {
            map.insert(edge[0].to_string(), vec![edge[1].to_string()]);
        }
        if map.contains_key(edge[1]) {
            map.get_mut(edge[1]).unwrap().push(edge[0].to_string());
        } else {
            map.insert(edge[1].to_string(), vec![edge[0].to_string()]);
        }
    }

    let mut paths = Tree::new();
    let mut visited: HashSet<String> = HashSet::new();
    let mut can_visit_small_cave = false;
    visited.insert("start".to_string());
    let i = paths.add_node(None, Node {
        val: "start".to_string(),
        children: vec![]
    });
    paths.set_root(Some(i));

    bfs(0, &map, &mut paths, &visited, &mut can_visit_small_cave);

    let mut path = vec![];
    let mut counter = 0;
    paths_from_tree(&mut counter, &mut path, 0, &mut paths);
    println!("Path Count: {}", counter);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut map: HashMap<String, Vec<String>> = HashMap::new();
    for line in input {
        let edge: Vec<&str> = line.trim().split("-").collect();
        if map.contains_key(edge[0]) {
            map.get_mut(edge[0]).unwrap().push(edge[1].to_string());
        } else {
            map.insert(edge[0].to_string(), vec![edge[1].to_string()]);
        }
        if map.contains_key(edge[1]) {
            map.get_mut(edge[1]).unwrap().push(edge[0].to_string());
        } else {
            map.insert(edge[1].to_string(), vec![edge[0].to_string()]);
        }
    }

    let mut paths = Tree::new();
    let mut visited: HashSet<String> = HashSet::new();
    let mut can_visit_small_cave = true;
    visited.insert("start".to_string());
    let i = paths.add_node(None, Node {
        val: "start".to_string(),
        children: vec![]
    });
    paths.set_root(Some(i));

    bfs(0, &map, &mut paths, &visited, &mut can_visit_small_cave);

    let mut path = vec![];
    let mut counter = 0;
    paths_from_tree(&mut counter, &mut path, 0, &mut paths);
    println!("Path Count: {}", counter);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn bfs(current_index: u32, map: &HashMap<String, Vec<String>>, paths: &mut Tree, visited: &HashSet<String>, can_visit_small_cave: &bool) {
    let current = paths.val_at(current_index).unwrap();
    if current == "end" {
        return;
    }
    let neighbors = map.get(&current).unwrap();
    for neighbor in neighbors {
        let mut visited = visited.clone();
        let mut can_visit_small_cave = can_visit_small_cave.clone();
        if (!visited.contains(neighbor) || can_visit_small_cave && visited.contains(neighbor)) &&
            neighbor != "start" {
            if can_visit_small_cave && visited.contains(neighbor) {
                can_visit_small_cave = false;
            }
            let child_index = paths.add_node(Some(current_index), Node {
                val: neighbor.to_string(),
                children: vec![]
            });
            if neighbor.to_lowercase() == neighbor.to_string() {
                visited.insert(neighbor.to_string());
            }
            bfs(child_index, map, paths, &visited, &can_visit_small_cave);
        }
    }
}

fn paths_from_tree(counter: &mut i32, path: &mut Vec<String>, current_index: u32, paths: &mut Tree) {
    let current = paths.node_at(current_index).unwrap();
    let children = current.children.clone();
    let val = current.val.to_string();
    if current.val == "end" {
        path.push(current.val.to_string());
        *counter += 1;
    }
    for c in children {
        let mut path = path.clone();
        path.push(val.to_string());
        paths_from_tree(counter, &mut path, c, paths);
    }
}
