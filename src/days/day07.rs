use crate::etc::utils::input;
use std::time::{ Instant };
use std::cmp;

pub fn run() {
    let input: Vec<String> = input::fetch_input("7".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut crabs: Vec<usize> = input[0].split(",")
        .map(|l| l.parse().expect("Not a number"))
        .collect();
    crabs.sort();
    let median = crabs[(&crabs.len() / 2)];
    let min_cost = fuel_cost(&crabs, median);
    println!("{}", min_cost);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let crabs: Vec<usize> = input[0].split(",")
        .map(|l| l.parse().expect("Not a number"))
        .collect();
    let mean: usize = (&crabs).iter().fold(0, |acc, n| acc + n) / &crabs.len();
    let min_cost = fuel_cost_expensive(&crabs, mean);
    println!("{}", min_cost);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

// Leaving this because this was how I originally solved the problem
fn _find_min_fuel_cost(
    crabs: &[usize],
    lo: usize,
    hi: usize,
    eval: fn(&[usize], usize) -> usize
) -> usize {
    if lo == hi {
        return eval(&crabs, lo);
    }
    let mid = (hi + lo) / 2;
    return cmp::min(
        _find_min_fuel_cost(&crabs, lo, mid, eval),
        _find_min_fuel_cost(&crabs, mid + 1, hi, eval)
    );
}

fn fuel_cost(crabs: &[usize], pos: usize) -> usize {
    return crabs.into_iter()
        .fold(0, |acc, crab| {
            acc + (crab.abs_diff(pos))
        })
}

fn fuel_cost_expensive(crabs: &[usize], pos: usize) -> usize {
    return crabs.into_iter()
        .fold(0, |acc, crab| {
            let n = crab.abs_diff(pos);
            acc + ((n * (n + 1)) / 2)
        })
}
