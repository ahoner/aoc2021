use crate::etc::utils::input;
use std::time::{ Duration, Instant };

use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::render::WindowCanvas;

#[derive(Debug)]
struct Bingo {
    board: Vec<Vec<i32>>,
    winning_sets: Vec<Vec<i32>>
}

enum WinningSet {
    Col(i32),
    Row(i32),
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("4".to_owned());
    part_1(&input);
    part_2(&input);
    animation(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut input_iter = input.iter();
    let mut bingo_boards: Vec<Bingo> = vec![];
    let numbers: Vec<i32> = input_iter.next()
        .unwrap()
        .trim()
        .split(",")
        .into_iter()
        .map(|s| s.parse().expect("Not a number"))
        .collect();
    input_iter.next();

    'parse: loop {
        let mut solutions: Vec<Vec<i32>> = vec![];
        // Row solutions
        for _i in 0..5 {
            let row_str = input_iter.next();
            match row_str {
                Some(s) => {
                    let row: Vec<i32> = s.trim()
                        .split(" ")
                        .filter(|s| *s != "")
                        .map(|s| s.parse().expect("Not a number"))
                        .collect();
                    solutions.push(row)
                },
                None => break 'parse
            }
        }
        // Col solutions
        for i in 0..5 {
            let col = vec![
                solutions[0][i],
                solutions[1][i],
                solutions[2][i],
                solutions[3][i],
                solutions[4][i],
            ];
            solutions.push(col);
        }
        bingo_boards.push(Bingo {
            board: solutions[0..5].to_vec(),
            winning_sets: solutions
        });
        input_iter.next();
    }

    let mut called_nums: Vec<i32> = vec![];
    let mut winner: Option<&Bingo> = None;
    'solution_check: for n in numbers {
        called_nums.push(n);
        for board in bingo_boards.iter() {
            if !check_board(&called_nums, &board).is_none() {
                winner = Some(board);
                break 'solution_check;
            }
        }
    }
    let score = calc_score(called_nums, winner.unwrap());

    println!("Time Elapsed: {}ms\nScore: {}", part_1_time.elapsed().as_micros() as f32 / 1000.0, score);
}

fn check_board(nums: &Vec<i32>, board: &Bingo) -> Option<WinningSet> {
    for (i, set ) in board.winning_sets.iter().enumerate() {
        if set.iter().all(|s| nums.contains(s)) && i < 5 {
            return Some(WinningSet::Row(i as i32));
        } else if set.iter().all(|s| nums.contains(s)) && 5 <= i && i < 10 {
            return Some(WinningSet::Col(i as i32 - 5));
        }
    }
    None
}

fn calc_score(mut nums: Vec<i32>, board: &Bingo) -> i32 {
    let mut board_set: Vec<i32> = vec![];
    for i in 0..5 {
        for n in board.winning_sets[i].iter() {
            board_set.push(*n);
        }
    }
    let sum = board_set.into_iter()
        .filter(|n| !nums.contains(n))
        .sum::<i32>();
    sum * nums.pop().unwrap()
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut input_iter = input.iter();
    let mut bingo_boards: Vec<Bingo> = vec![];
    let numbers: Vec<i32> = input_iter.next()
        .unwrap()
        .trim()
        .split(",")
        .into_iter()
        .map(|s| s.parse().expect("Not a number"))
        .collect();
    input_iter.next();

    'parse: loop {
        let mut solutions: Vec<Vec<i32>> = vec![];
        // Row solutions
        for _i in 0..5 {
            let row_str = input_iter.next();
            match row_str {
                Some(s) => {
                    let row: Vec<i32> = s.trim()
                        .split(" ")
                        .filter(|s| *s != "")
                        .map(|s| s.parse().expect("Not a number"))
                        .collect();
                    solutions.push(row)
                },
                None => break 'parse
            }
        }
        // Col solutions
        for i in 0..5 {
            let col = vec![
                solutions[0][i],
                solutions[1][i],
                solutions[2][i],
                solutions[3][i],
                solutions[4][i],
            ];
            solutions.push(col);
        }
        bingo_boards.push(Bingo {
            board: solutions[0..5].to_vec(),
            winning_sets: solutions
        });
        input_iter.next();
    }
    let mut called_nums: Vec<i32> = vec![];
    let mut winner: Option<&Bingo> = None;
    'reduce_solutions: for n in numbers.iter() {
        called_nums.push(*n);
        bingo_boards.drain_filter(|board| !check_board(&called_nums, &board).is_none());
        if bingo_boards.len() == 1 {
            break 'reduce_solutions;
        }
    }
    'solution_check: for n in numbers {
        called_nums.push(n);
        for board in bingo_boards.iter() {
            if !check_board(&called_nums, &board).is_none() {
                winner = Some(board);
                break 'solution_check;
            }
        }
    }
    let score = calc_score(called_nums, winner.unwrap());
    println!("Time Elapsed: {}ms\nSquid's Score: {}", part_2_time.elapsed().as_micros() as f32 / 1000.0, score);
}

fn animation(input: &Vec<String>) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("day 4", 800, 800)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;
    let mut input_iter = input.iter();
    let numbers: Vec<i32> = input_iter.next()
        .unwrap()
        .trim()
        .split(",")
        .into_iter()
        .map(|s| s.parse().expect("Not a number"))
        .collect();
    input_iter.next();

    let mut bingo_boards: Vec<Bingo> = vec![];
    'parse: loop {
        let mut solutions: Vec<Vec<i32>> = vec![];
        // Row solutions
        for _i in 0..5 {
            let row_str = input_iter.next();
            match row_str {
                Some(s) => {
                    let row: Vec<i32> = s.trim()
                        .split(" ")
                        .filter(|s| *s != "")
                        .map(|s| s.parse().expect("Not a number"))
                        .collect();
                    solutions.push(row)
                },
                None => break 'parse
            }
        }
        // Col solutions
        for i in 0..5 {
            let col = vec![
                solutions[0][i],
                solutions[1][i],
                solutions[2][i],
                solutions[3][i],
                solutions[4][i],
            ];
            solutions.push(col);
        }
        bingo_boards.push(Bingo {
            board: solutions[0..5].to_vec(),
            winning_sets: solutions
        });
        input_iter.next();
    }
    let mut called_nums: Vec<i32> = vec![];
    'game: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'game;
                },
                _ => {}
            }
        }
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        canvas.set_draw_color(Color::RGB(255, 255, 255));

        for (j, board) in bingo_boards.iter().enumerate() {
            draw_board(&mut canvas, &board, &called_nums, (j as i32 % 12) * 50, ((j as i32) / 12) * 50);
        }

        canvas.present();
        called_nums.push(numbers[i]);
        i += 1;
        ::std::thread::sleep(Duration::new(0, 200_000_000u32));
    }
}

fn draw_board(canvas: &mut WindowCanvas, board: &Bingo, called_nums: &Vec<i32>, x: i32, y: i32) {
    let spacing: i32 = 9;
    let board_er = vec![
        Point::new(x + 1, y + 1),
        Point::new(x + 49, y + 1),
        Point::new(x + 49, y + 49),
        Point::new(x + 1, y + 49),
        Point::new(x + 1, y + 1)
    ];
    for (i, row) in board.board.iter().enumerate() {
        for (j, num) in row.iter().enumerate() {
            canvas.set_draw_color(Color::RGB(255, 255, 255));
            if called_nums.contains(num) {
                canvas.set_draw_color(Color::RGB(0, 0, 255));
            }
            let rect = Rect::new((x + spacing * i as i32) + 6, ((y + 2) + spacing * j as i32) + 3, 4, 4);
            canvas.draw_rect(rect).expect("Draw Error");
        }
    }
    canvas.set_draw_color(Color::RGB(255, 255, 255));
    if let Some(winning_set) = check_board(&called_nums, &board) {
        match winning_set {
            WinningSet::Col(i) => {
                canvas.set_draw_color(Color::RGB(255, 0, 0));
                let start = Point::new(x + 1, (y + 6) + spacing * i);
                let end = Point::new(x + 49, (y + 6) + spacing * i);
                canvas.draw_line(start, end).expect("Draw Error");
            },
            WinningSet::Row(i) => {
                canvas.set_draw_color(Color::RGB(255, 0, 0));
                let start = Point::new((x + spacing * i) + 8, y + 1);
                let end = Point::new((x + spacing * i) + 8, y + 49);
                canvas.draw_line(start, end).expect("Draw Error");
            }
        }
        canvas.set_draw_color(Color::RGB(0, 255, 0));
    }
    canvas.draw_lines(&board_er[..]).expect("Draw Error");
    canvas.set_draw_color(Color::RGB(255, 255, 255));
}
