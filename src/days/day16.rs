use crate::etc::utils::input;
use crate::etc::parser::parser::{
    Parser,
    match_literal,
    pair,
    left,
    either,
    right,
    zero_or_more
};
use std::time::{ Instant };

#[derive(Debug)]
enum Packet {
    Literal(PacketLiteral),
    Operator(PacketOperator)
}

#[derive(Debug)]
struct PacketHeader {
    version: u8,
    type_id: u8,
}

#[derive(Debug)]
struct PacketLiteral {
    header: PacketHeader,
    literal: u64
}

#[derive(Debug)]
struct PacketOperator {
    header: PacketHeader,
    packets: Vec<Packet>
}


pub fn run() {
    let input: Vec<String> = input::fetch_input("16".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut bin_string = String::new();
    for c in input[0].chars() {
        bin_string.push_str(&hex_to_bin(c)[..]);
    }
    
    let parse_test = left(match_packet(), zero_or_more(match_literal("0")));
    let packet = parse_test.parse(&bin_string).map(|(_input, result)| result).unwrap();
    let version_sum = count_version(&packet);
    println!("Version Sum: {}", version_sum);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let mut bin_string = String::new();
    for c in input[0].chars() {
        bin_string.push_str(&hex_to_bin(c)[..]);
    }
    
    let parse_test = left(match_packet(), zero_or_more(match_literal("0")));
    let packet = parse_test.parse(&bin_string).map(|(_input, result)| result).unwrap();
    let evaluation = compute_packet_value(&packet);
    println!("Evalation: {}", evaluation);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn count_version(packet: &Packet) -> u32 {
    match packet {
        Packet::Literal(l) => l.header.version as u32,
        Packet::Operator(o) => o.header.version as u32 + o.packets.iter().fold(0, |acc, p| count_version(p) + acc)
    }
}

fn compute_packet_value(packet: &Packet) -> u64 {
    match packet {
        Packet::Literal(l) => l.literal,
        Packet::Operator(o) => {
            match o.header.type_id {
                0 => o.packets.iter().fold(0, |acc, p| acc + compute_packet_value(p)),
                1 => o.packets.iter().fold(1, |acc, p| acc * compute_packet_value(p)),
                2 => {
                    let mut min = u64::MAX;
                    for p in o.packets.iter() {
                        let v = compute_packet_value(p);
                        if min > v {
                            min = v
                        }
                    }
                    min
                },
                3 => {
                    let mut max = 0;
                    for p in o.packets.iter() {
                        let v = compute_packet_value(p);
                        if v > max {
                            max = v
                        }
                    }
                    max
                },
                5 => {
                    let v1 = compute_packet_value(&o.packets[0]);
                    let v2 = compute_packet_value(&o.packets[1]);
                    if v1 > v2 {
                        1
                    } else {
                        0
                    }
                },
                6 => {
                    let v1 = compute_packet_value(&o.packets[0]);
                    let v2 = compute_packet_value(&o.packets[1]);
                    if v2 > v1 {
                        1
                    } else {
                        0
                    }
                },
                7 => {
                    let v1 = compute_packet_value(&o.packets[0]);
                    let v2 = compute_packet_value(&o.packets[1]);
                    if v2 == v1 {
                        1
                    } else {
                        0
                    }
                }
                _ => 0
            }
        }
    }
}


fn match_bits<'a>(len: usize) -> impl Parser<'a, String> {
    move |input: &'a str| match input.get(0..len) {
        Some(next) => Ok((&input[len..], next.to_string())),
        _ => Err(input)
    }
}

fn match_header<'a>() -> impl Parser<'a, PacketHeader> {
    pair(match_bits(3), match_bits(3))
    .map(|(b1, b2)| PacketHeader {
        version: u8::from_str_radix(&b1, 2).unwrap(),
        type_id: u8::from_str_radix(&b2, 2).unwrap()
    })
}

fn match_packet_literal_groups<'a>() -> impl Parser<'a, u64> {
    pair(
        zero_or_more(
            match_bits(5)
                .pred(|b| b[..1] == *"1")
                .map(|group| {
                    let ret = &group[1..];
                    ret.to_string()
                })
        ),
        match_bits(5)
            .pred(|b| b[..1] == *"0")
            .map(|group| {
                let ret = &group[1..];
                ret.to_string()
            })
    )
    .map(|(groups, last_group)| {
        let mut ret = groups[..].to_vec();
        ret.push(last_group.to_string());
        let string = ret.join("");
        u64::from_str_radix(&string, 2).unwrap()
    })
}

fn match_packet_literal<'a>() -> impl Parser<'a, Packet> {
    pair(
        match_header().pred(|header| header.type_id == 4),
        match_packet_literal_groups()
    ).map(|(header, literal)| Packet::Literal(PacketLiteral {
        header,
        literal
    }))
}

fn num_sub_packet<'a, P, A>(parser: P, n: u32) -> impl Parser<'a, Vec<A>> 
    where P: Parser<'a, A> {
    move |mut input| {
        let mut result = Vec::new();
        if let Ok((next_input, first_item)) = parser.parse(input) {
            input = next_input;
            result.push(first_item);
        } else {
            return Err(input)
        }

        let mut count = 1;
        while n > count {
            if let Ok((next_input, next_item)) = parser.parse(input) {
                input = next_input;
                result.push(next_item);
            } else {
                return Err(input)
            }
            count += 1;
        }

        Ok((input, result))
    }
}


fn sub_packets_size<'a, P, A>(parser: P, n: usize) -> impl Parser<'a, Vec<A>> 
    where P: Parser<'a, A> {
    move |mut input| {
        let mut result = Vec::new();
        let mut len = 0;
        if let Ok((next_input, first_item)) = parser.parse(input) {
            len += input.len() - next_input.len();
            input = next_input;
            result.push(first_item);
        } else {
            return Err(input)
        }

        while n > len {
            if len > n {
                return Err(input)
            }
            if let Ok((next_input, next_item)) = parser.parse(input) {
                len += input.len() - next_input.len();
                input = next_input;
                result.push(next_item);
            } else {
                return Err(input)
            }
        }

        Ok((input, result))
    }
}

fn match_operator_length_packets<'a>() -> impl Parser<'a, Vec<Packet>> {
    match_bits(15)
        .map(|b| usize::from_str_radix(&b, 2).unwrap())
        .and_then(|n| {
            sub_packets_size(match_packet(), n)
        })
}

fn match_operator_num_packets<'a>() -> impl Parser<'a, Vec<Packet>> {
    match_bits(11)  
        .map(|b| u32::from_str_radix(&b, 2).unwrap())
        .and_then(|n| num_sub_packet(match_packet(), n))
}

fn match_packet_operater<'a>() -> impl Parser<'a, Packet> {
    pair(
        match_header(),
        either(
            right(
                match_bits(1).pred(|b| b[..1] == *"0"),
                match_operator_length_packets()
            ),
            right(
                match_bits(1).pred(|b| b[..1] == *"1"),
                match_operator_num_packets()
            )
        )
    ).map(|(header, packets)| Packet::Operator(PacketOperator {
        header,
        packets
    }))
}

fn match_packet<'a>() -> impl Parser<'a, Packet> {
    either(
        match_packet_literal(),
        match_packet_operater()
    )
}

fn hex_to_bin(c: char) -> String {
    match c {
        '0' => "0000".to_string(),
        '1' => "0001".to_string(),
        '2' => "0010".to_string(),
        '3' => "0011".to_string(),
        '4' => "0100".to_string(),
        '5' => "0101".to_string(),
        '6' => "0110".to_string(),
        '7' => "0111".to_string(),
        '8' => "1000".to_string(),
        '9' => "1001".to_string(),
        'A' => "1010".to_string(),
        'B' => "1011".to_string(),
        'C' => "1100".to_string(),
        'D' => "1101".to_string(),
        'E' => "1110".to_string(),
        'F' => "1111".to_string(),
        _ => "".to_string()
    }
}
