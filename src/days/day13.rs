use crate::etc::utils::input;
use std::time::{ Instant };
use std::collections::{ HashSet };

pub fn run() {
    let input: Vec<String> = input::fetch_input("13".to_owned());
    both_parts(&input);
}

fn both_parts(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let part_2_time = Instant::now();
    let mut points: HashSet<(u32, u32)> = HashSet::new();
    let mut folds: Vec<(char, u32)> = vec![];
    let mut iter = input.into_iter();

    'points: loop {
        let line = iter.next().unwrap().trim();
        if line.len() == 0 {
            break 'points;
        }
        let line: Vec<&str> = line.split(",")
            .collect();
        let point: (u32, u32) = (line[0].parse().unwrap(), line[1].parse().unwrap());
        points.insert(point);
    }
    'folds: loop {
        let line = iter.next();
        if line == None {
            break 'folds;
        }
        let line = line.unwrap().trim();
        let fold_line: Vec<&str> = line.split(" ").collect();
        let fold_str: Vec<&str> = fold_line[2].split("=").collect();
        folds.push((fold_str[0].chars().collect::<Vec<char>>()[0], fold_str[1].parse().unwrap()))
    }
    let mut first_fold = 0;
    for (axis, value) in folds {
        points = points.into_iter().fold(HashSet::new(), |mut acc, (x, y)| {
            match axis {
                'x' => {
                    if x > value {
                        let dist = x - value;
                        acc.insert((value - dist, y));
                    } else {
                        acc.insert((x, y));
                    }
                    acc
                },
                'y' => {
                    if y > value {
                        let dist = y - value;
                        acc.insert((x, (value - dist)));
                    } else {
                        acc.insert((x, y));
                    }
                    acc
                }
                _ => {
                    acc
                }
            }
        });
        if first_fold == 0 {
            first_fold = points.len();
            println!("Dots after first fold: {}", first_fold);
            println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
        }
    }
    let (x_max, y_max) = points.iter().fold((0, 0), |(mut x_max, mut y_max), (x, y)| {
        if x > &x_max {
            x_max = *x
        }
        if y > &y_max {
            y_max = *y
        }
        (x_max, y_max)
    });
    println!("- Part 2 -");
    for y in 0..y_max + 1 {
        let mut line = String::new();
        for x in 0..x_max + 1 {
            if points.contains(&(x, y)) {
                line.push('#');
            } else {
                line.push('.');
            }
        }
        println!("{}", line);
    }
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
