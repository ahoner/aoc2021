use crate::etc::utils::input;
use std::time::{ Instant };

#[derive(Debug, PartialEq)]
pub enum LineStatus {
    Ok,
    Corrupted(char),
    Incomplete(Vec<char>),
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("10".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut stack: Vec<char> = vec![];

    let score: u32 = input.iter()
        .map(|line| {
            let mut invalid: LineStatus = LineStatus::Ok;
            for c in line.chars() {
                match c {
                    '(' | '[' | '{' | '<' => stack.push(c),
                    ')' | ']' | '}' | '>' => {
                        let opener = stack.pop().unwrap();
                        if opener == '(' && c != ')' ||
                            opener == '[' && c != ']' ||
                            opener == '{' && c != '}' ||
                            opener == '<' && c != '>' {
                            invalid = LineStatus::Corrupted(c);
                        }
                    }
                    _ => println!("Invalid char")
                }
            }
            invalid
        })
        .filter(|o| match o {
            LineStatus::Corrupted(_) => true,
            _ => false
        })
        .map(|s| {
            if let LineStatus::Corrupted(o) = s {
                o
            } else {
                '_'
            }
        })
        .fold(0, |acc, c| {
            match c {
                ')' => acc + 3,
                ']' => acc + 57,
                '}' => acc + 1197,
                '>' => acc + 25137,
                _ => acc
            }
        });
    println!("{:?}", score);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();

    let mut scores: Vec<i64> = input.iter()
        .map(|line| {
            let mut invalid: LineStatus = LineStatus::Ok;
            let mut stack: Vec<char> = vec![];
            for c in line.chars() {
                match c {
                    '(' | '[' | '{' | '<' => stack.push(c),
                    ')' | ']' | '}' | '>' => {
                        let opener = stack.pop().unwrap();
                        if opener == '(' && c != ')' ||
                            opener == '[' && c != ']' ||
                            opener == '{' && c != '}' ||
                            opener == '<' && c != '>' {
                            invalid = LineStatus::Corrupted(c);
                        }
                    }
                    _ => println!("Invalid char")
                }
            }
            if invalid == LineStatus::Ok && stack.len() > 0 {
                invalid = LineStatus::Incomplete(stack)
            }
            invalid
        })
        .filter(|o| match o {
            LineStatus::Incomplete(_) => true,
            _ => false
        })
        .map(|s| match s {
            LineStatus::Incomplete(incomplete) => {
                incomplete.iter()
                    .rev()
                    .fold(0, |acc, c| {
                        match c {
                            '(' => acc * 5 + 1,
                            '[' => acc * 5 + 2,
                            '{' => acc * 5 + 3,
                            '<' => acc * 5 + 4,
                            _ => acc
                        }
                    })
            },
            _ => 0
        })
        .collect();

    scores.sort();
    let score = scores[scores.len() / 2];

    println!("{:?}", score);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
} 
