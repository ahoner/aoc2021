use crate::etc::utils::input;
extern crate sdl2;

use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;

use std::time::{ Duration, Instant };

pub fn run() {
    let input: Vec<String> = input::fetch_input("1".to_owned());
    part_1(&input);
    part_2(&input);
    animation(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let mut count = 0;
    let mut prev_depth = 0;
    for current_depth in input.iter() {
        let current_depth: u32 = current_depth.trim().parse().expect("depth not a number");
        if prev_depth != 0 && current_depth > prev_depth {
            count += 1;
        }
        prev_depth = current_depth;
    }
    println!("Time Elapsed: {}ms\nCount: {}", part_1_time.elapsed().as_micros() as f32 / 1000.0, count);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let mut i = 0;
    let mut prev_sum = 0;
    let mut count = 0;
    while i < input.len() - 3 {
        let a: u32 = input[i].trim().parse().expect("depth not a number");
        let b: u32 = input[i + 1].trim().parse().expect("depth not a number");
        let c: u32 = input[i + 2].trim().parse().expect("depth not a number");
        let sum = a + b + c;
        if prev_sum != 0 && sum > prev_sum {
            count += 1;
        }
        prev_sum = sum;
        i += 1;
    }
    println!("Time Elapsed: {}ms\nCount: {}", part_2_time.elapsed().as_micros() as f32 / 1000.0, count);
}

fn animation(input: &Vec<String>) {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("day 1", 800, 800)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut terrain: Vec<Point> = vec![];
    for (i, current_depth) in input.iter().enumerate() {
        let current_depth: i32 = current_depth.trim().parse().expect("depth not a number");
        let current_point = Point::new( i as i32 * 5, current_depth);
        terrain.push(current_point);
    }
    let mut i = 0;
    'game: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'game;
                },
                _ => {}
            }
        }
        let (_width, height) = canvas.output_size().expect("canvas error");
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        let offset = height as i32 / 2 - terrain[i].y;

        for p in terrain.iter_mut() {
            *p = p.offset(-5, offset as i32);
        }

        canvas.set_draw_color(Color::RGB(255, 255, 255));
        canvas.draw_lines(&terrain[..]).expect("could not draw point");

        canvas.present();
        i += 1;
        ::std::thread::sleep(Duration::new(0, 3_000_000_000u32 / 60));
    }

}
