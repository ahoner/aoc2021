use crate::etc::utils::input;
use std::time::{ Instant };

pub fn run() {
    let input: Vec<String> = input::fetch_input("6".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();
    let days = 80;
    let mut fish: Vec<i128> = vec![0; 9];
    input[0].trim()
        .split(",")
        .for_each(|age| {
            fish[age.parse::<usize>().expect("Not a number")] += 1
        });
    let final_day = (0..days).fold(fish.clone(), |mut acc: Vec<i128>, _i| {
        let num_zero = acc[0];
        (0..9 - 1).for_each(|i| acc[i] = acc[i + 1]);
        acc[6] += num_zero;
        acc[8] = num_zero;
        acc
    });
    println!("{:?}", final_day.iter().sum::<i128>());
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let days = 256;
    let mut fish: Vec<i128> = vec![0; 9];
    input[0].trim()
        .split(",")
        .for_each(|age| {
            fish[age.parse::<usize>().expect("Not a number")] += 1
        });
    let final_day = (0..days).fold(fish.clone(), |mut acc: Vec<i128>, _i| {
        let num_zero = acc[0];
        (0..9 - 1).for_each(|i| acc[i] = acc[i + 1]);
        acc[6] += num_zero;
        acc[8] = num_zero;
        acc
    });
    println!("{:?}", final_day.iter().sum::<i128>());
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}
