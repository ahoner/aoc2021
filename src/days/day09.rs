use crate::etc::utils::input;
use std::time::{ Instant };

#[derive(Debug)]
struct Tree {
    root: Option<u32>,
    arena: Vec<Option<Node>>
}

impl Tree {
    pub fn new() -> Self {
        Tree {
            arena: Vec::new(),
            root: None
        }
    }

    pub fn set_root(&mut self, root: Option<u32>) {
        self.root = root;
    }

    pub fn add_node(&mut self, parent: Option<u32>, node: Node) -> u32 {
        let i = self.arena.len() as u32;
        self.arena.push(Some(node));
        if parent == None {
            return 0;
        }
        let parent_node = self.node_at_mut(parent.unwrap()).unwrap();
        parent_node.children.push(i);
        return i;
    }

    pub fn node_at_mut(&mut self, index: u32) -> Option<&mut Node> {
        return if let Some(node) = self.arena.get_mut(index as usize) {
            node.as_mut()
        } else {
            None
        }
    }
    
    pub fn contains(&mut self, node: &Node) -> bool {
        let copy = Node {
            x: node.x,
            y: node.y,
            val: node.val,
            children: vec![],
        };
        self.arena.contains(&Some(copy))
    }

    pub fn count(&mut self) -> u32 {
        self.arena.len() as u32
    }
}

#[derive(Debug, Eq, std::hash::Hash)]
struct Node {
    x: u32,
    y: u32,
    val: u32,
    children: Vec<u32>
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

pub fn run() {
    let input: Vec<String> = input::fetch_input("9".to_owned());
    part_1(&input);
    part_2(&input);
}

fn part_1(input: &Vec<String>) {
    println!("- Part 1 -");
    let part_1_time = Instant::now();

    let lowpoints = find_lowpoints(input);
    let risk = lowpoints.iter()
        .fold(0, |acc, n| 1 + acc + n.val);
    println!("{:?}", risk);
    println!("Time Elapsed: {}ms", part_1_time.elapsed().as_micros() as f32 / 1000.0);
}

fn find_lowpoints(input: &Vec<String>) -> Vec<Node> {
    let heightmap: Vec<Vec<u32>> = input.iter()
        .map(|l| {
            l.chars()
                .map(|n| {
                    n.to_digit(10).unwrap()
                })
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();
    let mut lowpoints = vec![];
    for y in 0..heightmap.len() {
        for x in 0..heightmap[0].len() {
            let cur = heightmap[y][x];
            let mut neighbors = vec![];
            if y != 0 {
                neighbors.push(heightmap[y - 1][x]);
            }
            if heightmap[y].len() > x + 1 {
                neighbors.push(heightmap[y][x + 1]);
            }
            if heightmap.len() > y + 1 {
                neighbors.push(heightmap[y + 1][x]);
            }
            if x != 0 {
                neighbors.push(heightmap[y][x - 1]);
            }
            if neighbors.iter().fold(true, |acc, n| acc && n > &cur) {
                lowpoints.push(Node {
                    x: x as u32,
                    y: y as u32,
                    val:  cur,
                    children: vec![]
                });
            }
        }
    }
    return lowpoints
}

fn part_2(input: &Vec<String>) {
    println!("- Part 2 -");
    let part_2_time = Instant::now();
    let heightmap: Vec<Vec<u32>> = input.iter()
        .map(|l| {
            l.chars()
                .map(|n| {
                    n.to_digit(10).unwrap()
                })
                .collect::<Vec<u32>>()
        })
        .collect::<Vec<Vec<u32>>>();
    let lowpoints: Vec<Tree> = find_lowpoints(input).into_iter()
        .map(|lp| {
            let mut tree = Tree::new();
            let i = tree.add_node(None, Node {
                x: lp.x,
                y: lp.y,
                val: lp.val,
                children: vec![]
            });
            tree.set_root(Some(i));
            tree
        })
        .collect();
    let mut results = vec![];
    for mut t in lowpoints {
        flood_fill(&heightmap, 0, &mut t);
        results.push(t.count());
    }
    results.sort();
    let len = results.len();
    println!("{:?}", results[len - 1] * results[len - 2] * results[len - 3]);
    println!("Time Elapsed: {}ms", part_2_time.elapsed().as_micros() as f32 / 1000.0);
}

fn flood_fill(heightmap: &Vec<Vec<u32>>, cur_index: u32, tree: &mut Tree) {
    let cur = tree.node_at_mut(cur_index).unwrap();
    let x = cur.x as usize;
    let y = cur.y as usize;
    let mut neighbors = vec![];
    if y != 0 {
        neighbors.push(Node {
            x: x as u32,
            y: y as u32 - 1,
            val: heightmap[y - 1][x],
            children: vec![]
        });
    }
    if heightmap[y].len() > x + 1 {
        neighbors.push(Node {
            x: x as u32 + 1,
            y: y as u32,
            val: heightmap[y][x + 1],
            children: vec![]
        });
    }
    if heightmap.len() > y + 1 {
        neighbors.push(Node {
            x: x as u32,
            y: y as u32 + 1,
            val: heightmap[y + 1][x],
            children: vec![]
        });
    }
    if x != 0 {
        neighbors.push(Node {
            x: x as u32 - 1,
            y: y as u32,
            val: heightmap[y][x - 1],
            children: vec![]
        });
    }
    for i in 0..neighbors.len() {
        let cur_neighbor = Node {
            x: neighbors[i].x,
            y: neighbors[i].y,
            val: neighbors[i].val,
            children: vec![]
        };
        if cur_neighbor.val != 9 && !tree.contains(&cur_neighbor) {
            let child_index = tree.add_node(Some(cur_index), cur_neighbor);
            flood_fill(heightmap, child_index, tree);
        }
    }
}
