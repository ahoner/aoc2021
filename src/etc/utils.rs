pub mod input {
    use serde::Deserialize;

    use std::error::Error;
    use std::fs;
    use std::fs::File;
    use std::io::{self, BufRead, BufReader};
    use std::path::Path;

    #[derive(Deserialize, Debug)]
    struct Cookie {
       ga: String,
       gid: String,
       session: String 
    }

    pub fn fetch_input(day: String) -> Vec<String> {
        let input_path = format!("./inputs/day{}.input", day);
        let mut input = vec![];
        if !Path::new(&input_path).exists() {
            println!("Input does not exist");
            let cookie: Cookie= read_cookie().unwrap();
            let cookie = format!("_ga={}; _gid={}; session={}", cookie.ga, cookie.gid, cookie.session);

            let url = format!("https://adventofcode.com/2021/day/{}/input", day);
            let client = reqwest::blocking::Client::new();
            let body = client.get(url)
                .header("cookie", cookie)
                .send()
                .unwrap()
                .text();

            if let Ok(input_str) = body {
                fs::write(&input_path, input_str).expect("Could not read file");
            } 

        }

        if let Ok(lines) = read_lines(&input_path) {
            for line in lines {
                if let Ok(ip) = line {
                    input.push(ip);
                }
            }
        }

        return input;
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }

    fn read_cookie() -> Result<Cookie, Box<dyn Error>> {
        let json_file_path = Path::new("./cookie.json");
        let file = File::open(json_file_path)?;
        let reader = BufReader::new(file);
        let cookie = serde_json::from_reader(reader)?;

        Ok(cookie)
    }
}
